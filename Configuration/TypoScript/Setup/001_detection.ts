#############################
## Additional HTML Classes ##
#############################
config {
    htmlTag_stdWrap {
        setContentToCurrent = 1
        cObject = COA
        cObject {

            5 = TEXT
            5.data = TSFE:lang
            5.noTrimWrap = | lang="|" |

            10 = TEXT
            10.value = no-js
            10.noTrimWrap = |class="| |

            20 = USER
            20.userFunc = KITT3N\Kitt3nDetection\UserFunc\GetBrowserInfo->getBrowserInfo

            30 = TEXT
            30.value =
            30.noTrimWrap = ||" |

            wrap = <html | >
        }
    }
}

page {
    ## Set bMobile variable
    headerData {
        ## Script to replace no-js
        3320000 = TEXT
        3320000.value (

<script type="text/javascript">
    (function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);
</script>

        )

        ## Set variable kitt3n_bMobile
        3320010 = TEXT
        3320010.value (

<script type="text/javascript">
    var kitt3n_bMobile = {$plugin.tx_kitt3ndetection.settings.auto.bMobile};
</script>

        )
    }
}
<?php
declare(strict_types = 1);

namespace KITT3N\Kitt3nDetection\ExpressionLanguage\Functions;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Wolfcast\BrowserDetection;

// todo: make it work...
class IsMobileConditionFunctionsProvider implements ExpressionFunctionProviderInterface
{

    const COOKIE_NAME_BROWSER_DETECTION = "sBrowserDetection";
    const COOKIE_NAME_MOBILE_DETECTION = "bMobileDetection";

    /**
     * @return ExpressionFunction[] An array of Function instances
     */
    public function getFunctions()
    {
        return [
            $this->getMobileState()
        ];
    }

    // todo: make it work...
    /**
     * Shortcut function to access field values
     *
     * @return \Symfony\Component\ExpressionLanguage\ExpressionFunction
     */
    protected function getMobileState(): ExpressionFunction
    {
        return new ExpressionFunction('bMobile', function () {
            // Not implemented, we only use the evaluator
        }, function ($arguments) {

            $aCurrentApplicationContext = GeneralUtility::getApplicationContext();

            if ( ! isset($_COOKIE[self::COOKIE_NAME_MOBILE_DETECTION])) {
                $browser = new BrowserDetection();

                /*
                * Mobile?
                */
                $bMobile =  ($browser->isMobile() ? '1' : '0');
                /*
                 * Set Cookie if application context != Development
                 */
                if( ! $aCurrentApplicationContext->isDevelopment()) {
                    setcookie(self::COOKIE_NAME_MOBILE_DETECTION, $bMobile);
                }
            } else {
                $bMobile = $_COOKIE[self::COOKIE_NAME_MOBILE_DETECTION];
            }
            return $bMobile;

        });
    }
}
